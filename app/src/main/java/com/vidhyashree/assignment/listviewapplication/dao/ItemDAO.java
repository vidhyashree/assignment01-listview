package com.vidhyashree.assignment.listviewapplication.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.vidhyashree.assignment.listviewapplication.model.RowItem;
import com.vidhyashree.assignment.listviewapplication.typeconverters.Converters;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
@TypeConverters({Converters.class})
public interface ItemDAO {

    //Query to get all row items
    @TypeConverters(Converters.class)
    @Query("SELECT * FROM RowItem")
    LiveData<List<RowItem>> getAllItems();

    //insert all items
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<RowItem> rowItems);

    // Delete items
    @Query("DELETE FROM RowItem")
    void deleteAll();


    // update items
    @Update(onConflict = REPLACE)
    void updateList(List<RowItem> rowItems);

    @Query("SELECT COUNT(*) FROM RowItem")
    Integer getCount();


   /* @Query("SELECT COUNT(isSelected) FROM RowItem WHERE isSelected = 'false'")
    int getSelected();*/

    @Query("SELECT COUNT(*) FROM RowItem WHERE isSelected = 'true'")
    int getSelected();



    @Query("SELECT * FROM RowItem WHERE isSelected IN (:bool)")
     List<RowItem> getSelected(boolean bool);

}

package com.vidhyashree.assignment.listviewapplication.observer;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import android.util.Log;




    public class MyLifecycleObserver implements LifecycleObserver {

        public static final String TAG = MyLifecycleObserver.class.getSimpleName();

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        public void create() {
            //This method is called AFTER LifcycleOwner(Activity/Fragment) onCreate() is called
            Log.e(TAG, "onCreate: ");
        }


        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void start() {
            //This method is called AFTER LifcycleOwner(Activity/Fragment) onStart() is called
            Log.e(TAG, "onStart: ");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void resume() {
            //This method is called AFTER LifcycleOwner(Activity/Fragment) onResume() is called
            Log.e(TAG, "onResume: ");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void pause() {
            //This method is called BEFORE LifcycleOwner(Activity/Fragment) onPause() is called
            Log.e(TAG, "onPause: ");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void stop() {
            //This method is called BEFORE LifcycleOwner(Activity/Fragment) onStop() is called
            Log.e(TAG, "onStop: ");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void destroy() {
            //This method is called BEFORE LifcycleOwner(Activity/Fragment) onDestroy() is called
            Log.e(TAG, "onDestroy: ");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
        public void any() {
            //This method is called on any Lifcycle changes happens
            Log.e(TAG, "onAny: ");
        }


}

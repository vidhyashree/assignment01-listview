package com.vidhyashree.assignment.listviewapplication;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import com.vidhyashree.assignment.listviewapplication.utils.NetworkConnectivityReceiver;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    private NetworkConnectivityReceiver myReceiver;

    private NetworkConnectivityReceiver getMyReceiver() {
        if (myReceiver == null) {
            myReceiver = new NetworkConnectivityReceiver();
        }
        return myReceiver;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        registerReceiver();
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(NetworkConnectivityReceiver.NetworkConnectivityListener listener) {
        NetworkConnectivityReceiver.connectivityReceiverListener = listener;
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(getMyReceiver(), filter);


    }
}

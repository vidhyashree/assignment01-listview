package com.vidhyashree.assignment.listviewapplication.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.vidhyashree.assignment.listviewapplication.R;
import com.vidhyashree.assignment.listviewapplication.fragment.ListViewFragment;
import com.vidhyashree.assignment.listviewapplication.observer.MyLifecycleObserver;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new ListViewFragment()).commit();
        }


    }
}

package com.vidhyashree.assignment.listviewapplication.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vidhyashree.assignment.listviewapplication.R;
import com.vidhyashree.assignment.listviewapplication.model.RowItem;

import java.util.ArrayList;
import java.util.List;


public class RowItemAdapter extends RecyclerView.Adapter<RowItemAdapter.ViewHolder> {


    private List<RowItem> rowList;
    private Context mContext;

    private ICart cartListener;


    public RowItemAdapter(Context context, ICart mlistener) {
        this.rowList=new ArrayList<>();
        cartListener = mlistener;
        mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        private TextView mTitle;
        private TextView mDescription;
        private ImageView mImage;
        private CheckBox mCheckbox;

        ViewHolder(View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.row_title);
            mDescription = itemView.findViewById(R.id.row_description);
            mImage = itemView.findViewById(R.id.row_image);
            mCheckbox = itemView.findViewById(R.id.checkbox);


        }

        void bind(final RowItem rowItem) {
            if (rowItem != null) {
                mTitle.setText(rowItem.getTitle());
                mDescription.setText(rowItem.getDescription());

                mCheckbox.setChecked(rowItem.getSelected());

                //image loading using Glide
                Glide.with(mContext).load(rowItem.getImageHref()).into(mImage);

                mCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        rowItem.setSelected(b);
                        cartListener.updateCartInfo();

                    }
                });

            }
        }



    }

    @NonNull
    @Override
    public RowItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RowItemAdapter.ViewHolder viewHolder, int position) {

        viewHolder.bind(rowList.get(position));

    }

    @Override
    public int getItemCount() {
        return rowList.size();
    }


    public void addItems(List<RowItem> borrowModelList) {
        if (rowList != null) {
           /* PostDiffCallback postDiffCallback = new PostDiffCallback(data, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(postDiffCallback);*/

            rowList.clear();
            rowList.addAll(borrowModelList);
           // diffResult.dispatchUpdatesTo(this);
        } else {
            // first initialization
            rowList = borrowModelList;
        }

    }


}

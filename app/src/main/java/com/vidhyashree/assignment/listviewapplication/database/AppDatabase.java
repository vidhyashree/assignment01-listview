package com.vidhyashree.assignment.listviewapplication.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import com.vidhyashree.assignment.listviewapplication.dao.ItemDAO;
import com.vidhyashree.assignment.listviewapplication.model.RowItem;


@Database(entities = {RowItem.class}, exportSchema = false,version = 1)


public abstract class AppDatabase extends RoomDatabase{


    public abstract ItemDAO getDAO();


    private static AppDatabase DB_INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (DB_INSTANCE == null) {


            //Database is created
            DB_INSTANCE =Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "item_db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return DB_INSTANCE;
    }

}

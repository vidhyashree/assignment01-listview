package com.vidhyashree.assignment.listviewapplication.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.vidhyashree.assignment.listviewapplication.typeconverters.Converters;

import java.util.ArrayList;

/*Model class for ListItems*/

@Entity
public class ListItem {



    @PrimaryKey(autoGenerate = true)
    public int id;

    private String title;

    @Embedded
    @TypeConverters(Converters.class)
    private ArrayList<RowItem> rows;


    public ListItem(int id, String title, ArrayList<RowItem> rows) {
        this.id = id;
        this.title = title;
        this.rows.addAll(rows);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<RowItem> getRows() {
        if (rows == null) {
            rows = new ArrayList<>();
            return rows;
        }
        return rows;

    }

    public void setRows(ArrayList<RowItem> rows) {
        this.rows = rows;
    }
}

package com.vidhyashree.assignment.listviewapplication.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;

import com.vidhyashree.assignment.listviewapplication.dao.ItemDAO;
import com.vidhyashree.assignment.listviewapplication.database.AppDatabase;
import com.vidhyashree.assignment.listviewapplication.model.RowItem;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ViewModel extends AndroidViewModel {

    private AppDatabase appDatabase;
    private ExecutorService executorService;

    public ViewModel(Application context) {
        super(context);
        appDatabase = AppDatabase.getDatabase(context);
        executorService = Executors.newSingleThreadExecutor();

    }

    public LiveData<List<RowItem>> getAllRows() {
        return appDatabase.getDAO().getAllItems();
    }

    public void insertItems(List<RowItem> items) {
        executorService.execute(() -> appDatabase.getDAO().insertAll(items));
       // new insertAsyncTask(appDatabase.getDAO()).execute(items);

    }

    public Integer getSelectedCount(){
        try {
            new getSelectedCount(appDatabase.getDAO()).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer getCount() {
        try {
            return new getCount(appDatabase.getDAO()).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public LiveData<List<RowItem>> getAllItems() {
        try {
            return new getAllItems(appDatabase.getDAO()).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void update(List<RowItem> items) {
        appDatabase.getDAO().updateList(items);
    }


    //Asynctask to insert items
    private static class insertAsyncTask extends AsyncTask<List<RowItem>, Void, Void> {

        private ItemDAO mAsyncTaskDao;

        insertAsyncTask(ItemDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<RowItem>... params) {
            mAsyncTaskDao.insertAll(params[0]);
            return null;
        }
    }

    //Asynctask to delete all items
    private static class getAllItems extends AsyncTask<Void, Void, LiveData<List<RowItem>>> {
        private ItemDAO mAsyncTaskDao;

        getAllItems(ItemDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected LiveData<List<RowItem>> doInBackground(Void... voids) {

            return  mAsyncTaskDao.getAllItems();
        }
    }

    //Asynctask to get count of items
    private static class getCount extends AsyncTask<Void, Void, Integer> {

        private ItemDAO mAsyncTaskDao;

        getCount(ItemDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Void... params) {
            return mAsyncTaskDao.getCount();
        }
    }

    //Asynctask to get count of items
    private static class getSelectedCount extends AsyncTask<Void, Void, Integer> {

        private ItemDAO mAsyncTaskDao;

        getSelectedCount(ItemDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Void... params) {
            return mAsyncTaskDao.getSelected();
        }
    }


}

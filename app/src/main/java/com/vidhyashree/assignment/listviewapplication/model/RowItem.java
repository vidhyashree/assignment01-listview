package com.vidhyashree.assignment.listviewapplication.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class RowItem {

    @PrimaryKey(autoGenerate = true)
    public int rowId;

    private String title;
    private String description;
    private String imageHref;
    private boolean isSelected;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }
}

package com.vidhyashree.assignment.listviewapplication.fragment;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vidhyashree.assignment.listviewapplication.MyApplication;
import com.vidhyashree.assignment.listviewapplication.R;
import com.vidhyashree.assignment.listviewapplication.adapter.ICart;
import com.vidhyashree.assignment.listviewapplication.adapter.RowItemAdapter;
import com.vidhyashree.assignment.listviewapplication.interfaces.APIInterface;
import com.vidhyashree.assignment.listviewapplication.model.ListItem;
import com.vidhyashree.assignment.listviewapplication.model.RowItem;
import com.vidhyashree.assignment.listviewapplication.observer.MyLifecycleObserver;
import com.vidhyashree.assignment.listviewapplication.utils.NetworkConnectivityReceiver;
import com.vidhyashree.assignment.listviewapplication.utils.RetroFitAPIClient;
import com.vidhyashree.assignment.listviewapplication.viewmodel.ViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.ViewGroup.*;

/**
 * A placeholder fragment containing a simple view.
 */
public class ListViewFragment extends Fragment implements NetworkConnectivityReceiver.NetworkConnectivityListener, ICart {

    private ViewModel mListViewModel;
    private RowItemAdapter recyclerViewAdapter;


    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeContainer;

    @BindView(R.id.empty_list_message)
    TextView message;
    @BindView(R.id.count)
    Button count;

    @BindView(R.id.location_layout)
    LinearLayout locationsLayout;

    private Context context;

    int enabledButton;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();

        //This is how you add LifeCycleObserver
        getLifecycle().addObserver(new MyLifecycleObserver());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_listview, container, false);
        ButterKnife.bind(this, mRootView);

        initUI();
        initRefreshLayout();
        return mRootView;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initUI() {

        recyclerViewAdapter = new RowItemAdapter(context, this);

        mListViewModel = ViewModelProviders.of(getActivity()).get(ViewModel.class);
        mListViewModel.getAllItems().observe((LifecycleOwner) context, posts -> recyclerViewAdapter.addItems(posts));
        populateData();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewAdapter);

        ArrayList<String> list = new ArrayList<String>();
        // List<String> list;

        list.add("All");
        list.add("rack1");
        list.add("rack3");
        list.add("rack2");
        list.add("rack4rack4rack4rack4rack4rack4rack4");

        RadioGroup radiogroup = new RadioGroup(getContext());
        RadioButton[] radiobutton = new RadioButton[list.size()];
        radiogroup.setOrientation(LinearLayout.HORIZONTAL);


        for (int i = 0; i < list.size(); i++) {


           /* radiobutton[i] = new RadioButton(getContext());
            radiobutton[i].setText(list.get(i));
            radiobutton[i].setId(i);
            radiobutton[i].setButtonDrawable(null);

            radiobutton[i].setTextColor(Color.WHITE);
            radiogroup.removeView(radiobutton[i]);
            radiogroup.addView(radiobutton[i]);
            radiobutton[i].setBackground(getContext().getDrawable(R.drawable.selector));
            radiobutton[i].setTextColor(getContext().getColor(R.color.color_item_title));*/


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(15, 0, 15, 0);

           TextView btn=new TextView(getContext());
         /*   View layout = LayoutInflater.from(context).inflate(R.layout.custom_location_bg, null, false);
            Button btn = layout.findViewById(R.id.locationName);*/



            btn.setLayoutParams(params);
            btn.setGravity(Gravity.CENTER);

            btn.setPadding(30,0,30,0);

            btn.setText(list.get(i));
            btn.setId(i);
            btn.setBackground(getContext().getDrawable(R.drawable.backgroun_btn_white));
            btn.setTextColor(Color.parseColor("#ffffff"));


            btn.setOnClickListener(view -> {
                view.setSelected(true);
                view.setBackground(getContext().getDrawable(R.drawable.backgroun_btn));

                ((TextView)view).setTextColor(Color.WHITE);
                enabledButton = view.getId();


                for (int j = 0; j < list.size(); j++) {
                    if (enabledButton != j) {
                        locationsLayout.findViewById(j).setSelected(false);
                        locationsLayout.findViewById(j).setBackground(getContext().getDrawable(R.drawable.backgroun_btn_white));
                        ((TextView)locationsLayout.findViewById(j)).setTextColor(Color.parseColor("#000000"));
                    }
                }
            });

            locationsLayout.addView(btn);




        }
        locationsLayout.getChildAt(0).performClick();
       /* locationsLayout.addView(radiogroup);
        radiobutton[1].setChecked(true);

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int checkedRadioButtonId = radiogroup.getCheckedRadioButtonId();
                RadioButton radioBtn = (RadioButton) radiogroup.findViewById(checkedRadioButtonId);
                // radioBtn.setBackground(getContext().getDrawable(R.drawable.backgroun_btn));
                Toast.makeText(getContext(), radioBtn.getText(), Toast.LENGTH_SHORT).show();

            }
        });*/


    }

    public void deselectButtons(List<String> list,View view) {
        for (int i = 0; i < list.size(); i++) {
            if (enabledButton != i)
                locationsLayout.findViewById(i).setSelected(false);
        }
    }


    private void populateData() {

        if (NetworkConnectivityReceiver.isConnected())
            fetchDataFromAPI();


    }

    private void fetchDataFromAPI() {

        Call<ListItem> responseObj = RetroFitAPIClient.getRetrofitInstance().create(APIInterface.class).getAllItems();

        responseObj.enqueue(new Callback<ListItem>() {
            @Override
            public void onResponse(@NonNull Call<ListItem> call, Response<ListItem> response) {

                if (response.isSuccessful()) {
                    addToDB(response.body());


                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(getContext(), "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(getContext(), "not logged in or server broken", Toast.LENGTH_SHORT).show();

                            break;
                        default:
                            Toast.makeText(getContext(), "unknown error", Toast.LENGTH_SHORT).show();

                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListItem> call, Throwable t) {
                Toast.makeText(getContext(), "Request Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addToDB(ListItem item) {
        mListViewModel.insertItems(item.getRows());
        recyclerViewAdapter.notifyDataSetChanged();
        swipeContainer.setRefreshing(false);
        message.setVisibility(mListViewModel.getCount() > 1 ? View.GONE : View.VISIBLE);
        //   recyclerView.setVisibility(mListViewModel.getCount() > 1 ? View.VISIBLE : View.GONE);
    }


    private void initRefreshLayout() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //code to refresh the list.
                if (NetworkConnectivityReceiver.isConnected()) {

                    fetchDataFromAPI();
                } else {
                    swipeContainer.setRefreshing(false);
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = "Connected to Internet!";
        } else {
            message = "Internet Unavailable!";
        }

        Snackbar snackbar = Snackbar
                .make(getActivity().findViewById(R.id.snackbar_view), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
        // populateData();
    }


    @Override
    public void updateCartInfo() {
        count.setText(String.valueOf(mListViewModel.getSelectedCount()));
    }
}

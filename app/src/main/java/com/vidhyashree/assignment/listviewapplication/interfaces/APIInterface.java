package com.vidhyashree.assignment.listviewapplication.interfaces;

import com.vidhyashree.assignment.listviewapplication.model.ListItem;
import com.vidhyashree.assignment.listviewapplication.model.RowItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {



    @GET("s/2iodh4vg0eortkl/facts.json")
    Call<ListItem> getAllItems();

}

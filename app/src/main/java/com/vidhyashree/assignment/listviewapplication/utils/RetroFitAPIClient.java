package com.vidhyashree.assignment.listviewapplication.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetroFitAPIClient {

        private static final String BASE_URL = "https://dl.dropboxusercontent.com/";

        private static Retrofit retrofit = null;


        public static Retrofit getRetrofitInstance() {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }

}
